package config

import (
	"github.com/kelseyhightower/envconfig"
	"github.com/sirupsen/logrus"
)

const (
	dnmCfgPrefix = "DNM"
)

type (
	Config struct {
		Runtime
		Server
	}

	Runtime struct {
		IsDev    bool   `envconfig:"DNM_IS_DEVELOPMENT" default:"false"`
		LogLevel string `envconfig:"DNM_LOG_LEVEL" default:"info"`
	}

	Server struct {
		Addr string `envconfig:"DNM_SERVER_ADDRESS" default:":14005"`
	}
)

func LoadEnvConfig() *Config {
	cfg := &Config{}
	if err := envconfig.Process(dnmCfgPrefix, cfg); err != nil {
		logrus.Panicf("config: failed to load environment config: %s", err)
	}
	return cfg
}
