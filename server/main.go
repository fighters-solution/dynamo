package main

import (
	"context"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
	"github.com/rs/cors"
	"github.com/sirupsen/logrus"
	"gitlab.com/fighters-solution/dynamo/lib"
	"gitlab.com/fighters-solution/dynamo/lib/model"
	"gitlab.com/fighters-solution/dynamo/lib/util"
	"gitlab.com/fighters-solution/dynamo/server/config"
	"gopkg.in/yaml.v2"

	// Imports generators
	_ "gitlab.com/fighters-solution/dynamo/lib/languages/c"
	_ "gitlab.com/fighters-solution/dynamo/lib/languages/cpp"
	_ "gitlab.com/fighters-solution/dynamo/lib/languages/golang"
	_ "gitlab.com/fighters-solution/dynamo/lib/languages/java"
	_ "gitlab.com/fighters-solution/dynamo/lib/languages/javascript"
	_ "gitlab.com/fighters-solution/dynamo/lib/languages/kotlin"
	_ "gitlab.com/fighters-solution/dynamo/lib/languages/python"
)

var (
	defaultLangs = []string{
		lib.C,
		lib.Cpp,
		lib.Go,
		lib.Java,
		lib.Javascript,
		lib.Kotlin,
		lib.Python3,
	}
)

func main() {
	cfg := config.LoadEnvConfig()

	// Logging
	lvl, _ := logrus.ParseLevel(cfg.Runtime.LogLevel)
	logrus.SetLevel(lvl)

	r := chi.NewRouter()
	r.Use(middleware.DefaultLogger, middleware.Recoverer)

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello! API is being served at: POST /api/v1/gen"))
	})
	r.Route("/api/v1", func(r chi.Router) {
		r.Post("/gen", generateTestCases)
	})

	srv := &http.Server{
		Addr:    cfg.Server.Addr,
		Handler: cors.Default().Handler(r),
	}
	stopChan := make(chan os.Signal, 1)
	errChan := make(chan error, 1)
	signal.Notify(stopChan, os.Interrupt, os.Kill, syscall.SIGTERM, syscall.SIGKILL)
	go func() {
		logrus.Infof("main: server is running at %s", cfg.Server.Addr)
		errChan <- srv.ListenAndServe()
	}()

	select {
	case <-stopChan:
		logrus.Infof("main: signal received. Exiting")
	case err := <-errChan:
		logrus.Infof("main: error received: %s. Exiting", err)
	}
	eCtx, ctxCancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer ctxCancel()
	_ = srv.Shutdown(eCtx)
	logrus.Info("main: server stopped")
}

func generateTestCases(w http.ResponseWriter, r *http.Request) {
	langs := defaultLangs
	if parm := r.URL.Query().Get("langs"); parm != "" {
		langs = strings.Split(parm, ",")
	}

	in := model.Input{}
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if err := yaml.Unmarshal(b, &in); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	ret := make([]*model.GeneratedTest, 0)
	for _, l := range langs {

		lang := strings.ToLower(strings.TrimSpace(l))
		gen, err := lib.Generate(util.CloneInputObject(in), lang)
		if err != nil {
			logrus.Errorf("gen: failed to gen %s: %s", lang, err)
			continue
		}
		ret = append(ret, gen)
	}
	render.JSON(w, r, ret)
}
