# Dynamo

Dynamo help to generate the code template and fixtures (test cases) for Code Fights challenges in many programming languages.  
Current supported languages and its test framework are:

1. C 11 (Criterion). `c`
2. C++ 14 (Igloo). `cpp`
3. Go 1.8 (Ginkgo). `go`
4. Java 8 (JUnit). `java`
5. Javascript 8.1.3 (cw-2). `javacript`
6. Kotlin 1.1.4 (JUnit 4). `kotlin`
7. Python 3.6 (cw-2). `python3`

### Structure

The repository has three main folders:

1. `lib`:  Actual Dynamo source code for code generation written as a library.
2. `cmd`: CLI tool to generate code from console.
3. `server`: An HTTP server serves a REST API for code generation.

The two folders `cmd` and `server` are ready to go. You just have to build the CLI tool or start the server to start generating code.  
All other modifications in code generation should be made in `lib` folder.

### Build from source

1. You must have Go SDK installed and configured `GOROOT`/`GOPATH` correctly.

2. Clone code.

   ```shell
   $ go get gitlab.com/fighters-solution/dynamo
   ```

3. Install `dep` tool.

   ```shell
   $ go get -u github.com/golang/dep/cmd/dep
   ```

4. Install dependencies.

   ```shell
   $ cd $GOPATH/gitlab.com/fighters-solution/dynamo
   $ dep ensure -v
   ```

5. Build CLI tool.

   ```shell
   $ cd cmd/
   $ go build -o dynamo main.go
   ```

   CLI tool usage:

   ```shell
   -f: Path to YAML input file.
   -l: List of programming languages to generate test cases, separated by comma.
   -o: Path to output file, if not specify then output to stdout.

   Examples:
   dynamo -f input.yaml   // Generate code for all supported languages, output to stdout.
   dynamo -f input.yaml -o code_gen.json   // Generate code for all supported languages, output to file.
   dynamo -f input.yaml -o code_gen.json -l "c,go,python3"   // Generate code for C, Go and Python 3.
   ```

6. Build server.

   ```shell
   $ cd ../server
   $ go build
   $ ./server
   ```

   Run the compiled `server` binary to start the generation server on default port `14005`.

### Notes for developer

1. You should also install the [`realize`](https://github.com/tockins/realize) tool for live-reloading the `server` when developing code generation API.

   ```shell
   $ go get github.com/tockins/realize
   $ cd $GOPATH/gitlab.com/fighters-solution/dynamo/server
   $ realize start
   ```
   The `realize` dashboard will serve on port `5002`.

2. If you're adding new programming language generation, remember to import the language generator in `cmd/main.go` and `server/main.go` so the the generator can be registered before generating code.

3. Remember to pass the cloned object of YAML input (`model.Input`) instead of the input itself to `lib.Generate()`.  

   ```
   lib.Generate(util.CloneInputObject(in), lang)
   ```

   The reason is the input object contains slices which will be referenced when passing into `lib.Generate`. Some languages have to normalize the input data (E.g.: Converting `true/false` boolean to `True/False` in Python or `1/0` in C/C++) which will change the value of slices.  
   Those changed values causing invalid code gen in later language generations, so we have to use a separated input objects (cloned from original input) for each language generation.

4. New supported language generations must pass all templates inside `examples` folder.

### License

This project is under the [MIT License](https://opensource.org/licenses/MIT).
