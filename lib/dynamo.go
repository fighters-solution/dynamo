package lib

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"gitlab.com/fighters-solution/dynamo/lib/model"
)

type Generator interface {
	Generate(in model.Input) (*model.GeneratedTest, error)
}

const (
	C          = "c"
	Cpp        = "cpp"
	Go         = "go"
	Java       = "java"
	Javascript = "javascript"
	Kotlin     = "kotlin"
	Python3    = "python3"
)

var (
	langsMap = make(map[string]Generator)
)

func Register(lang string, gen Generator) {
	if _, ok := langsMap[lang]; ok {
		logrus.Panicf("dynamo: %s generator registered twice", lang)
	}
	langsMap[lang] = gen
	logrus.Debugf("dynamo: %s generator registered", lang)
}

func Generate(in model.Input, lang string) (*model.GeneratedTest, error) {
	g, ok := langsMap[lang]
	if !ok {
		return nil, fmt.Errorf("not found %s generator. Is it registered yet?", lang)
	}
	return g.Generate(in)
}
