package model

type (
	Input struct {
		Function  Function
		Fixtures  TestCase
		TestCases TestCase
	}

	Function struct {
		Name    string
		Params  []string
		Returns []string
	}

	TestCase struct {
		Describe string
		Cases    []Case
	}

	Case struct {
		It       string
		Args     []string
		Expected []string
	}

	GeneratedTest struct {
		Language string `json:"language"`
		Template string `json:"template"`
		Fixture  string `json:"fixture"`
		TestCase string `json:"testCase"`
	}
)

type (
	InternalFunc struct {
		Imports string
		Name    string
		Param   string
		Return  string
	}

	InternalTestCase struct {
		Imports  string
		Describe string
		Cases    string
	}

	Param struct {
		Name      string
		Type      string
		IsPointer bool // For C, CPP, Go
		IsArray   bool
		IsBool    bool // For C, CPP, Python
	}
)
