package util

import (
	"bytes"
	"io/ioutil"
	"regexp"
	"strings"

	"gitlab.com/fighters-solution/dynamo/lib/model"
)

var (
	spCharsRegex = regexp.MustCompile("[^a-zA-Z0-9_ ]+")
)

func ToCamelCase(s string, isExported bool) string {
	if len(s) == 0 {
		return s
	}
	ret := bytes.Buffer{}
	s = strings.TrimPrefix(strings.TrimSuffix(s, "_"), "_")
	if isExported {
		ret.WriteString(strings.ToUpper(string(s[0])))
	} else {
		ret.WriteString(string(s[0]))
	}
	for i := 1; i < len(s); i++ {
		if string(s[i]) == " " {
			continue
		}
		if string(s[i]) == "_" {
			ret.WriteString(strings.ToUpper(string(s[i+1])))
			i++
			continue
		}
		ret.WriteString(string(s[i]))
	}
	return ret.String()
}

func ToSnakeCase(s string) string {
	if len(s) == 0 {
		return s
	}
	ret := bytes.Buffer{}
	s = strings.TrimPrefix(strings.TrimSuffix(s, "_"), "_")
	ret.WriteString(strings.ToLower(string(s[0])))
	for i := 1; i < len(s); i++ {
		if string(s[i]) == " " {
			ret.WriteString("_")
			continue
		}
		if s[i] > 64 && s[i] < 91 {
			if string(s[i-1]) != "_" {
				ret.WriteString("_")
			}
			ret.WriteString(strings.ToLower(string(s[i])))
			continue
		}
		ret.WriteString(string(s[i]))
	}
	return ret.String()
}

func WriteFile(f string, data []byte) error {
	return ioutil.WriteFile(f, data, 0644)
}

func UpperFirstChar(s string) string {
	if len(s) == 0 {
		return s
	}
	return strings.ToUpper(string(s[0])) + string(s[1:])
}

func RemoveSpecialChars(s string) string {
	return spCharsRegex.ReplaceAllString(s, "")
}

// Input model has array field which referenced when passing to lib.Generate.
// That's why we have to clone new input object for each language generation.
func CloneInputObject(in model.Input) model.Input {
	clonedIn := in

	clonedIn.Function.Params = make([]string, len(in.Function.Params))
	copy(clonedIn.Function.Params, in.Function.Params)
	clonedIn.Function.Returns = make([]string, len(in.Function.Returns))
	copy(clonedIn.Function.Returns, in.Function.Returns)

	clonedIn.Fixtures.Cases = make([]model.Case, len(in.Fixtures.Cases))
	copy(clonedIn.Fixtures.Cases,  in.Fixtures.Cases)
	for i := range in.Fixtures.Cases {
		clonedIn.Fixtures.Cases[i].Args = make([]string, len(in.Fixtures.Cases[i].Args))
		copy(clonedIn.Fixtures.Cases[i].Args, in.Fixtures.Cases[i].Args)
		clonedIn.Fixtures.Cases[i].Expected = make([]string, len(in.Fixtures.Cases[i].Expected))
		copy(clonedIn.Fixtures.Cases[i].Expected, in.Fixtures.Cases[i].Expected)
	}

	clonedIn.TestCases.Cases = make([]model.Case, len(in.TestCases.Cases))
	copy(clonedIn.TestCases.Cases,  in.TestCases.Cases)
	for i := range in.TestCases.Cases {
		clonedIn.TestCases.Cases[i].Args = make([]string, len(in.TestCases.Cases[i].Args))
		copy(clonedIn.TestCases.Cases[i].Args, in.TestCases.Cases[i].Args)
		clonedIn.TestCases.Cases[i].Expected = make([]string, len(in.TestCases.Cases[i].Expected))
		copy(clonedIn.TestCases.Cases[i].Expected, in.TestCases.Cases[i].Expected)
	}

	return clonedIn
}
