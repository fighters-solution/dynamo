package kotlin

import (
	"bytes"
	"fmt"
	"strings"
	"text/template"

	"github.com/sirupsen/logrus"
	"gitlab.com/fighters-solution/dynamo/lib"
	"gitlab.com/fighters-solution/dynamo/lib/model"
	"gitlab.com/fighters-solution/dynamo/lib/util"
)

var (
	tmpl   *template.Template
	tcTmpl *template.Template
)

type gen struct{}

func init() {
	var err error
	tmpl, err = template.New("template").Parse(codeTmpl)
	if err != nil {
		logrus.Panicf("js: failed to parse template: %s", err)
	}
	tcTmpl, err = template.New("testCaseTemplate").Parse(testTmpl)
	if err != nil {
		logrus.Panicf("js: failed to parse test case template: %s", err)
	}
	g := &gen{}
	lib.Register(lib.Javascript, g)
}

func (g *gen) Generate(in model.Input) (*model.GeneratedTest, error) {
	// Template
	gen := &model.GeneratedTest{
		Language: lib.Javascript,
	}

	pArgs, err := parseParams(in.Function.Params)
	if err != nil {
		return nil, err
	}
	pRets, err := parseParams(in.Function.Returns)
	if err != nil {
		return nil, err
	}

	tmpl, err := genTemplate(&in.Function, pArgs, pRets)
	if err != nil {
		return nil, err
	}
	gen.Template = tmpl
	// Fixture
	fixture, err := genTestCase(&in, pArgs, pRets, false)
	if err != nil {
		return nil, err
	}
	gen.Fixture = fixture
	// Test case
	tc, err := genTestCase(&in, pArgs, pRets, true)
	if err != nil {
		return nil, err
	}
	gen.TestCase = tc
	return gen, nil
}

func genTemplate(f *model.Function, pArgs []model.Param, pRets []model.Param) (string, error) {
	iFunc, err := parseFunction(f, pArgs, pRets)
	if err != nil {
		return "", err
	}
	b := &bytes.Buffer{}
	tmpl.Execute(b, iFunc)
	return b.String(), nil
}

func parseFunction(f *model.Function, pArgs []model.Param, pRets []model.Param) (*model.InternalFunc, error) {
	iFunc := &model.InternalFunc{
		Name: util.ToCamelCase(f.Name, true),
	}
	params := ""
	for _, p := range pArgs {
		params += fmt.Sprintf("%s, ", p.Name)
	}
	iFunc.Param = strings.TrimSuffix(params, ", ")
	iFunc.Return = pRets[0].Name
	return iFunc, nil
}

func genTestCase(in *model.Input, pArgs []model.Param, pRets []model.Param, isTestCase bool) (string, error) {
	iTc := &model.InternalTestCase{}
	desc := in.Fixtures.Describe
	if isTestCase {
		desc = in.TestCases.Describe
	}
	if desc == "" {
		desc = util.ToCamelCase(in.Function.Name, true)
	}

	iTc.Describe = desc

	fName := util.ToCamelCase(in.Function.Name, true)
	for i, c := range in.Fixtures.Cases {
		tc, err := generateCase(c, fName, pArgs, pRets, i)
		if err != nil {
			return "", err
		}
		iTc.Cases += tc + "\n"
	}
	if isTestCase {
		for i, c := range in.TestCases.Cases {
			tc, err := generateCase(c, fName, pArgs, pRets, i+len(in.Fixtures.Cases))
			if err != nil {
				return "", err
			}
			iTc.Cases += tc + "\n"
		}
	}
	iTc.Cases = strings.TrimSuffix(iTc.Cases, "\n\n")

	b := &bytes.Buffer{}
	tcTmpl.Execute(b, iTc)
	return b.String(), nil
}

func parseParams(params []string) ([]model.Param, error) {
	ret := make([]model.Param, 0)

	for i, p := range params {
		spl := strings.Split(p, " ") // a int
		pName, pType := "", ""
		switch {
		case len(spl) == 1:
			pName = fmt.Sprintf("p%d", i)
			pType = spl[0]
		case len(spl) == 2:
			pName = spl[0]
			pType = spl[1]
		default:
			return nil, fmt.Errorf("js: invalid function parameter: %s", p)
		}

		pArgs := model.Param{
			Name: pName,
		}
		pArgs.Type, pArgs.IsArray = getActualType(pType)
		ret = append(ret, pArgs)
	}
	return ret, nil
}

func getActualType(t string) (string, bool) {
	isArray := false
	if strings.HasSuffix(t, "[]") {
		isArray = true
	}
	return t, isArray
}

func generateCase(c model.Case, fName string, pArgs []model.Param, pRets []model.Param, idx int) (string, error) {
	baseTabs := "\t\t"
	ret := fmt.Sprintf("%s// Test case #%d\n", baseTabs, idx)
	if c.It == "" {
		c.It = fmt.Sprintf("Test case #%d", idx)
	}
	ret += fmt.Sprintf("%sTest.it(\"%s\", function() {\n", baseTabs, c.It)
	for i, a := range c.Args {
		iName := fmt.Sprintf("%d%d", idx, i)
		ret += fmt.Sprintf("%s\tvar in%s = %s;\n", baseTabs, iName, a)
	}
	ret += fmt.Sprintf("%s\tvar ret%d = %s(", baseTabs, idx, fName)
	for i := range c.Args {
		ret += fmt.Sprintf("in%d%d, ", idx, i)
	}
	ret = strings.TrimSuffix(ret, ", ") + ");\n"
	ret += fmt.Sprintf("%s\tvar ex%d = %s;\n", baseTabs, idx, c.Expected[0])
	if pRets[0].IsArray {
		ret += fmt.Sprintf("%s\tTest.assertSimilar(ret%d, ex%d);\n", baseTabs, idx, idx)
	} else {
		ret += fmt.Sprintf("%s\tTest.assertEquals(ret%d, ex%d);\n", baseTabs, idx, idx)
	}
	ret += fmt.Sprintf("%s});\n", baseTabs)
	return ret, nil
}

// Templates
var (
	codeTmpl = `function {{.Name}}({{.Param}}) {
  // your code here

}
`
	testTmpl = `Test.describe("{{.Describe}}", function() {
{{.Cases}}
});
`
)
