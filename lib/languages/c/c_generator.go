package c

import (
	"bytes"
	"fmt"
	"strings"
	"text/template"

	"github.com/sirupsen/logrus"
	"gitlab.com/fighters-solution/dynamo/lib"
	"gitlab.com/fighters-solution/dynamo/lib/model"
	"gitlab.com/fighters-solution/dynamo/lib/util"
)

var (
	typesMap        map[string]string
	unknownTypesMsg []string

	tmpl   *template.Template
	tcTmpl *template.Template
)

type gen struct{}

func init() {
	var err error
	tmpl, err = template.New("template").Parse(codeTmpl)
	if err != nil {
		logrus.Panicf("c: failed to parse template: %s", err)
	}
	tcTmpl, err = template.New("testCaseTemplate").Parse(testTmpl)
	if err != nil {
		logrus.Panicf("c: failed to parse test case template: %s", err)
	}
	initTypes()
	g := &gen{}
	lib.Register(lib.C, g)
}

func initTypes() {
	typesMap = make(map[string]string)
	typesMap["byte"] = "char"
	typesMap["short"] = "short"
	typesMap["int"] = "int"
	typesMap["long"] = "long"
	typesMap["float"] = "float"
	typesMap["double"] = "double"
	typesMap["boolean"] = "int"
	typesMap["string"] = "char*"
	// Pointer: *type
	// Array: type[]
	// Both: *type[]
}

func (g *gen) Generate(in model.Input) (*model.GeneratedTest, error) {
	// Template
	unknownTypesMsg = make([]string, 0)
	gen := &model.GeneratedTest{
		Language: lib.C,
	}

	pArgs, err := parseParams(in.Function.Params)
	if err != nil {
		return nil, err
	}
	pRets, err := parseParams(in.Function.Returns)
	if err != nil {
		return nil, err
	}
	normalizeTestCases(&in, pArgs, pRets)

	tmpl, err := genTemplate(&in.Function, pArgs, pRets)
	if err != nil {
		return nil, err
	}
	gen.Template = tmpl

	// Fixture
	unknownTypesMsg = make([]string, 0)
	fixture, err := genTestCase(&in, pArgs, pRets, false)
	if err != nil {
		return nil, err
	}
	gen.Fixture = fixture

	// Test case
	unknownTypesMsg = make([]string, 0)
	tc, err := genTestCase(&in, pArgs, pRets, true)
	if err != nil {
		return nil, err
	}
	gen.TestCase = tc
	return gen, nil
}

func genTemplate(f *model.Function, pArgs []model.Param, pRets []model.Param) (string, error) {
	iFunc, err := parseFunction(f, pArgs, pRets)
	if err != nil {
		return "", err
	}
	b := &bytes.Buffer{}
	tmpl.Execute(b, iFunc)
	if len(unknownTypesMsg) != 0 {
		return fmt.Sprintf("// TODO: Please define or correct unknown type(s): %s\n\n",
			strings.Join(unknownTypesMsg, ", ")) + b.String(), nil
	}
	return b.String(), nil
}

func parseFunction(f *model.Function, pArgs []model.Param, pRets []model.Param) (*model.InternalFunc, error) {
	iFunc := &model.InternalFunc{
		Name: util.ToSnakeCase(f.Name),
	}
	params := ""
	for _, p := range pArgs {
		if p.IsArray {
			params += fmt.Sprintf("%s %s[], int %s, ", p.Type, p.Name, p.Name+"_size")
			continue
		}
		params += fmt.Sprintf("%s %s, ", p.Type, p.Name)
	}
	iFunc.Param = strings.TrimSuffix(params, ", ")

	if len(pRets) == 0 {
		iFunc.Return = "void"
	} else {
		iFunc.Return = pRets[0].Type
		if pRets[0].IsArray {
			iFunc.Return += "*"
		}
	}
	return iFunc, nil
}

func genTestCase(in *model.Input, pArgs []model.Param, pRets []model.Param, isTestCase bool) (string, error) {
	iFc, err := parseFunction(&in.Function, pArgs, pRets)
	if err != nil {
		return "", err
	}

	iTc := &model.InternalTestCase{}
	desc := in.Fixtures.Describe
	if isTestCase {
		desc = in.TestCases.Describe
	}
	if desc == "" {
		desc = util.ToSnakeCase(in.Function.Name)
	}
	desc = util.ToSnakeCase(desc) // TODO

	fName := util.ToSnakeCase(in.Function.Name)
	for i, c := range in.Fixtures.Cases {
		tc, err := generateCase(c, pArgs, pRets, fName, i)
		if err != nil {
			return "", err
		}
		iTc.Cases += tc + "\n"
	}
	if isTestCase {
		for i, c := range in.TestCases.Cases {
			tc, err := generateCase(c, pArgs, pRets, fName, i+len(in.Fixtures.Cases))
			if err != nil {
				return "", err
			}
			iTc.Cases += tc + "\n"
		}
	}
	iTc.Cases = strings.TrimSuffix(iTc.Cases, "\n\n")

	h := fmt.Sprintf("%s %s(%s);", iFc.Return, iFc.Name, iFc.Param)
	b := &bytes.Buffer{}
	tcTmpl.Execute(b, struct {
		Header string
		T      *model.InternalTestCase
	}{h, iTc})
	if len(unknownTypesMsg) != 0 {
		return fmt.Sprintf("// TODO: Please define or correct unknown type(s): %s\n\n",
			strings.Join(unknownTypesMsg, ", ")) + b.String(), nil
	}
	return b.String(), nil
}

func parseParams(params []string) ([]model.Param, error) {
	ret := make([]model.Param, 0)

	for i, p := range params {
		spl := strings.Split(p, " ") // a int
		pName, pType := "", ""
		switch {
		case len(spl) == 1:
			pName = fmt.Sprintf("p%d", i)
			pType = spl[0]
		case len(spl) == 2:
			pName = spl[0]
			pType = spl[1]
		default:
			return nil, fmt.Errorf("c: invalid function parameter: %s", p)
		}

		pArgs := model.Param{
			Name: pName,
		}
		getActualType(&pArgs, pType)
		ret = append(ret, pArgs)
	}
	return ret, nil
}

func getActualType(arg *model.Param, t string) {
	if strings.HasPrefix(t, "*") {
		arg.IsPointer = true
		t = strings.TrimPrefix(t, "*")
	}
	if strings.HasSuffix(t, "[]") {
		arg.IsArray = true
		t = strings.TrimSuffix(t, "[]")
	}
	if strings.Contains(t, "boolean") {
		arg.IsBool = true
	}

	if v, ok := typesMap[t]; ok {
		t = v
	} else {
		unknownTypesMsg = append(unknownTypesMsg, t)
	}
	if arg.IsPointer {
		t += "*"
	}
	arg.Type = t
}

func generateCase(c model.Case, pArgs []model.Param, pRets []model.Param, fName string, idx int) (string, error) {
	baseTabs := "\t"
	ret := fmt.Sprintf("// Test case #%d\n", idx)
	if c.It == "" {
		c.It = fmt.Sprintf("Test case #%d", idx)
	}
	ret += fmt.Sprintf("Test(%s, %s) {\n", fName, util.ToSnakeCase(util.RemoveSpecialChars(c.It)))
	for i, a := range c.Args {
		iName := fmt.Sprintf("%d%d", idx, i)
		if pArgs[i].IsArray {
			a = strings.TrimPrefix(strings.TrimSuffix(a, "]"), "[")
			ret += fmt.Sprintf("%s%s in%s[] = {%s};\n", baseTabs, pArgs[i].Type, iName, a)
		} else {
			ret += fmt.Sprintf("%s%s in%s = %s;\n", baseTabs, pArgs[i].Type, iName, a)
		}
	}

	if pRets[0].IsArray {
		ret += fmt.Sprintf("%s%s* ret%d = %s(", baseTabs, pRets[0].Type, idx, fName)
	} else {
		ret += fmt.Sprintf("%s%s ret%d = %s(", baseTabs, pRets[0].Type, idx, fName)
	}
	for i := range c.Args {
		iName := fmt.Sprintf("%d%d", idx, i)
		if pArgs[i].IsPointer {
			ret += fmt.Sprintf("&in%s, ", iName)
			continue
		}
		if pArgs[i].IsArray {
			ret += fmt.Sprintf("in%s, %d, ", iName, getArraySize(c.Args[i]))
			continue
		}
		ret += fmt.Sprintf("in%s, ", iName)
	}
	ret = strings.TrimSuffix(ret, ", ") + ");\n"
	if pRets[0].IsArray {
		tmpEx := strings.TrimPrefix(strings.TrimSuffix(c.Expected[0], "]"), "[")
		ret += fmt.Sprintf("%s%s ex%d[] = {%s};\n", baseTabs, pRets[0].Type, idx, tmpEx)
	} else {
		ret += fmt.Sprintf("%s%s ex%d = %s;\n", baseTabs, pRets[0].Type, idx, c.Expected[0])
	}

	if pRets[0].Type == "char*" { // string
		ret += fmt.Sprintf("%scr_assert_str_eq(ret%d, ex%d);\n", baseTabs, idx, idx)
	} else if pRets[0].Type == "float" || pRets[0].Type == "double" {
		ret += fmt.Sprintf("%scr_assert_float_eq(ret%d, ex%d, 0.0001);\n", baseTabs, idx, idx)
	} else if pRets[0].IsArray {
		ret += fmt.Sprintf("%scr_assert_arr_eq(ret%d, ex%d, %d * sizeof(%s));\n", baseTabs, idx, idx, getArraySize(c.Expected[0]), pRets[0].Type)
	} else {
		ret += fmt.Sprintf("%scr_assert_eq(ret%d, ex%d);\n", baseTabs, idx, idx)
	}
	ret += "}\n"
	return ret, nil
}

func normalizeTestCases(in *model.Input, pArgs []model.Param, pRets []model.Param) {
	for i, a := range pArgs {
		if a.IsBool {
			for j := range in.Fixtures.Cases {
				in.Fixtures.Cases[j].Args[i] = strings.Replace(in.Fixtures.Cases[j].Args[i], "false", "0", -1)
				in.Fixtures.Cases[j].Args[i] = strings.Replace(in.Fixtures.Cases[j].Args[i], "true", "1", -1)
			}
			for j := range in.TestCases.Cases {
				in.TestCases.Cases[j].Args[i] = strings.Replace(in.TestCases.Cases[j].Args[i], "false", "0", -1)
				in.TestCases.Cases[j].Args[i] = strings.Replace(in.TestCases.Cases[j].Args[i], "true", "1", -1)
			}
		}
	}

	for i, a := range pRets {
		if a.IsBool {
			for j := range in.Fixtures.Cases {
				in.Fixtures.Cases[j].Expected[i] = strings.Replace(in.Fixtures.Cases[j].Expected[i], "false", "0", -1)
				in.Fixtures.Cases[j].Expected[i] = strings.Replace(in.Fixtures.Cases[j].Expected[i], "true", "1", -1)
			}
			for j := range in.TestCases.Cases {
				in.TestCases.Cases[j].Expected[i] = strings.Replace(in.TestCases.Cases[j].Expected[i], "false", "0", -1)
				in.TestCases.Cases[j].Expected[i] = strings.Replace(in.TestCases.Cases[j].Expected[i], "true", "1", -1)
			}
		}
	}
}

func getArraySize(s string) int {
	if s == "[]" {
		return 0
	}
	return strings.Count(s, ",") + 1
}

// Templates
var (
	codeTmpl = `{{.Return}} {{.Name}}({{.Param}}) {
  // your code here

}
`
	testTmpl = `#include <criterion/criterion.h>
{{.T.Imports}}
{{.Header}}

{{.T.Cases}}
`
)
