package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"os"
	"path"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/fighters-solution/dynamo/lib"
	"gitlab.com/fighters-solution/dynamo/lib/model"
	"gitlab.com/fighters-solution/dynamo/lib/util"
	"gopkg.in/yaml.v2"

	// Imports generators
	_ "gitlab.com/fighters-solution/dynamo/lib/languages/c"
	_ "gitlab.com/fighters-solution/dynamo/lib/languages/cpp"
	_ "gitlab.com/fighters-solution/dynamo/lib/languages/golang"
	_ "gitlab.com/fighters-solution/dynamo/lib/languages/java"
	_ "gitlab.com/fighters-solution/dynamo/lib/languages/javascript"
	_ "gitlab.com/fighters-solution/dynamo/lib/languages/kotlin"
	_ "gitlab.com/fighters-solution/dynamo/lib/languages/python"
)

var (
	fInput = flag.String("f", "", "Input YAML file")
	fLangs = flag.String("l", "", "List of programming languages to generate test cases, separated by comma")
	fOut   = flag.String("o", "", "Path to output file, if not specify then output to stdout")

	defaultLangs = []string{
		lib.C,
		lib.Cpp,
		lib.Go,
		lib.Java,
		lib.Javascript,
		lib.Kotlin,
		lib.Python3,
	}
)

func main() {
	flag.Parse()

	if *fInput == "" {
		pwd, _ := os.Getwd()
		if _, err := os.Stat(path.Join(pwd, "input.yaml")); os.IsNotExist(err) {
			logrus.Error("main: please specify input file or create default input.yaml file in current working directory")
			return
		}
		*fInput = path.Join(pwd, "input.yaml")
	}
	b, err := ioutil.ReadFile(*fInput)
	if err != nil {
		logrus.Errorf("main: input file not found: %s", err)
		return
	}

	in := model.Input{}
	err = yaml.Unmarshal(b, &in)
	if err != nil {
		logrus.Errorf("main: failed to parse input file: %s", err)
		return
	}
	//js, _ := json.MarshalIndent(in, "", "  ")
	//logrus.Infof("YAML: %s\n", string(js))

	langs := defaultLangs
	if *fLangs != "" {
		langs = strings.Split(*fLangs, ",")
	}
	ret := make([]*model.GeneratedTest, 0)
	for _, l := range langs {
		lang := strings.ToLower(strings.TrimSpace(l))
		gen, err := lib.Generate(util.CloneInputObject(in), lang)
		if err != nil {
			logrus.Errorf("main: failed to generate test cases for %s: %s", lang, err)
			continue
		}
		ret = append(ret, gen)
	}

	js, _ := json.MarshalIndent(ret, "", "  ")
	if *fOut != "" {
		if err := util.WriteFile(*fOut, js); err != nil {
			logrus.Errorf("main: failed to write result: %s", err)
			return
		}
		logrus.Infof("main: Result has ben written to file: %s", *fOut)
		return
	}
	logrus.Infof("\n%s", string(js))
}
