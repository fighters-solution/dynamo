PROJECT_NAME=dynamo
BUILD_VERSION=$(shell cat VERSION)
DOCKER_IMAGE=$(PROJECT_NAME):$(BUILD_VERSION)
GO_BUILD_ENV=CGO_ENABLED=0 GOOS=linux GOARCH=amd64
GO_FILES=$(shell go list ./... | grep -v /vendor/)

.SILENT:

all: fmt vet test install

# Build Dynamo server
build:
	cd server; \
	$(GO_BUILD_ENV) go build -v -o $(PROJECT_NAME)-$(BUILD_VERSION).bin .

install:
	$(GO_BUILD_ENV) go install

vet:
	go vet $(GO_FILES)

fmt:
	go fmt $(GO_FILES)

test:
	go test $(GO_FILES) -cover -v

integration_test:
	go test -tags=integration $(GO_FILES) -cover -v

docker: vet test build
	mv server/$(PROJECT_NAME)-$(BUILD_VERSION).bin $(PROJECT_NAME).bin; \
	docker build -t $(DOCKER_IMAGE) .; \
	rm -rf $(PROJECT_NAME).bin 2> /dev/null

docker_run:
	docker run -p 14005:14005 $(DOCKER_IMAGE)
