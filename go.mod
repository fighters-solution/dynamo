module gitlab.com/fighters-solution/dynamo

go 1.12

require (
	github.com/go-chi/chi v3.3.4+incompatible
	github.com/go-chi/render v1.0.1
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/rs/cors v0.0.0-20170727213201-7af7a1e09ba3
	github.com/sirupsen/logrus v1.4.2
	golang.org/x/crypto v0.0.0-20180126023034-0efb9460aaf8 // indirect
	gopkg.in/yaml.v2 v2.2.2
)
